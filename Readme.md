## Country Information Search

Given an input, a country aplha2code or alpha3code, it outputs the country information such as its capital, timezones,population etc.
It uses gtkmm to draw windows and restcountries api for country data. JSON are download at the program startup

### MainWindow Class
It is situated in window.cpp file and is a subclass of gtk window class. [Gtk page](https://developer-old.gnome.org/gtkmm/stable/classGtk_1_1Window.html#details). It is a top level window which contain other 
widgets such as a button, label. In this app it contains a box(vertical one). A Box is a container widget that pack widgets in either vertical or horizontal position in this app it contains a label, an entry
and a button widget. When a user enter a country alpha code such "in or ind" for India in the entry, a signal clicked is generated on the button widget and then a lambda function is executed. This function
creates a new information window that display country information.

### Country Data
This one stores the json data from restcountries and searches for the specified input country code.

### Country Info Model
It is a counry information display window.

### fetchDetails()
It is situated in exec.cpp file. It downloads json data using curl command.

### Compiling
Compile using the command 

``g++ window.cpp CountryInfoModel.cpp exec.cpp `pkg-config gtkmm-3.0 --cflags --libs` ``

It requires gtk3 devel, gtkmm packages.
