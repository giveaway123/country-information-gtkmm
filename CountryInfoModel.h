#ifndef COUNTRYINFOMODEL_H
#define COUNTRYINFOMODEL_H

#include <gtkmm.h>
#include "nlohmann/json.hpp"
#include "exec.h"
#include <string>
using json =  nlohmann::json;
class CountryInfo : public Gtk::Window {
	public:
		CountryInfo(json country);
		virtual ~CountryInfo();
	protected:
		class Model : public Gtk::TreeModel::ColumnRecord {
			public:
				Model() { 
					add(capital), add(country), add(population), 
						add(languages), add(region), add(timezones);
				   	}
				Gtk::TreeModelColumn<Glib::ustring> country;
				Gtk::TreeModelColumn<Glib::ustring> capital;
				Gtk::TreeModelColumn<Glib::ustring> population;
				Gtk::TreeModelColumn<Glib::ustring> languages;
				Gtk::TreeModelColumn<Glib::ustring> region;
				Gtk::TreeModelColumn<Glib::ustring> timezones;
		};
		Model m_Columns;
		Gtk::TreeView m_tree;
		Glib::RefPtr<Gtk::ListStore> m_refTreeModel;	
};

#endif
