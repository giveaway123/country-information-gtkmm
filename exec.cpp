#include "exec.h"
#include <cstdio>
#include <unistd.h>
using namespace std;
using json = nlohmann::json;

json fetchDetails() {
	//Create named pipe with given name
	int ret = mkfifo("/tmp/fifo", S_IRWXU);
	//file descriptor for open function
	int fd;
	//creates a new process and if child is less than 0 then fork() fails
	int child = fork();
	if (child < 0) {
		cout << "Fork failed " << endl;
		return 1;
		//child process
	} else if (child == 0) {
		//open named pipe write end and duplicates its descriptor with stdout
		fd = open("/tmp/fifo",O_WRONLY);
		dup2(fd,STDOUT_FILENO);
		//execute curl command
		int ret = execlp("curl", "curl", "https://restcountries.eu/rest/v2/all",NULL);
		//if we reach here, it will mean that execlp failed to execute curl process
		cout << "Unable to execute the process\n";
		
	}
	//main process
	fd = open("/tmp/fifo", O_RDONLY);
	string data;
	char ch;
	//read until END OF PIPE
	while(read(fd, &ch, 1) == 1 ) {
		data += ch;
	}
	//parse json
	json d;
	if (!data.empty()) {
		d = json::parse(data);
	} 
	close(fd);
	unlink("/tmp/fifo");
	return d;
}
