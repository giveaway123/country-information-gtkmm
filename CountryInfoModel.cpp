#include "CountryInfoModel.h"
#include "glibmm/ustring.h"

using json = nlohmann::json;

CountryInfo::CountryInfo(json country) {
	set_title("Query");
	set_default_size(400, 200);
	m_refTreeModel = Gtk::ListStore::create(m_Columns);
	m_tree.set_model(m_refTreeModel);

	Gtk::TreeModel::Row row = *(m_refTreeModel->append());
	row[m_Columns.capital] = country["capital"].dump();
	row[m_Columns.country] = country["name"].dump();
	row[m_Columns.population] = country["population"].dump();
	row[m_Columns.region] = country["region"].dump();
	row[m_Columns.timezones] = country["timezones"].dump();

	m_tree.append_column("Capital",m_Columns.capital);
	m_tree.append_column("Country",m_Columns.country);
	m_tree.append_column("Population",m_Columns.population);
	m_tree.append_column("Region",m_Columns.region);
	m_tree.append_column("Timezones",m_Columns.timezones);

	add(m_tree);
	show_all_children();
}

CountryInfo::~CountryInfo() {}
