#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <sys/wait.h>
#include "nlohmann/json.hpp"

using json = nlohmann::json;

json fetchDetails();
