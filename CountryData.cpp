#include "exec.h"
#include "nlohmann/json.hpp"
#include <algorithm>
#include <cctype>
#include <iostream>

class CountryData {
	public:
		CountryData();
		nlohmann::json search(std::string);
	private:
		nlohmann::json countries;
};

CountryData::CountryData() {
	countries = fetchDetails();
}

nlohmann::json CountryData::search(std::string query) {
	nlohmann::json temp;
	for (auto d : countries) {
		if ((d["alpha3Code"] == query) || (d["alpha2Code"] == query))
			return d;
	}
	return temp;
}
