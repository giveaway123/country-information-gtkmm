#include <algorithm>
#include <gtkmm.h>
#include <string>
#include <iostream>
#include <memory>
#include "CountryInfoModel.h"
#include "CountryData.cpp"
class MainWindow : public Gtk::Window {
	public:
		MainWindow();
		virtual ~MainWindow();

	protected:
		Gtk::Button search;
		Gtk::Label name;
		Gtk::Box vbox;
		Gtk::ButtonBox bbox;
		Gtk::Entry entry;
		std::unique_ptr<CountryInfo> n;
	private:
		CountryData *jsonData;
};

MainWindow::MainWindow() : search("search"), vbox(Gtk::ORIENTATION_VERTICAL,20){
	set_title("Search Country");
	set_size_request(300,200);	
	name.set_label("Please enter Your country name!");
	search.show();
	vbox.show();
	entry.show();
	bbox.add(search);
	bbox.show();
	vbox.pack_start(name,Gtk::PACK_SHRINK);
	vbox.pack_start(entry,Gtk::PACK_SHRINK);
	vbox.pack_start(bbox,Gtk::PACK_SHRINK);
	//search button signal
	search.signal_clicked().connect([this] {
			if (entry.get_text() == "") {
				name.set_label("Enter a country name to continue!");	
				return;
			}	
			search.set_state(Gtk::STATE_INSENSITIVE);
			std::string q = entry.get_text();
			std::transform(q.begin(),q.end(),q.begin(),::toupper);
			json country = jsonData->search(q);				
			n = std::unique_ptr<CountryInfo>(new CountryInfo(country));
			n->signal_hide().connect([&] {
				search.set_state(Gtk::STATE_NORMAL);
				n.reset();
			});	
			n->grab_focus();
			n->show_all();	
		});
		//download restcountries json 	
		jsonData = new CountryData();
	add(vbox);
	show_all();
}
MainWindow::~MainWindow() {}
int main(int argc,char *argv[]) {
	auto app =  Gtk::Application::create(".example.org.CountryInformation");
	MainWindow m;
	return app->run(m);
}
